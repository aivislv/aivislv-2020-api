export const albumDef = `
 type Album @cacheControl(maxAge: 604800) {
    id: Int!,
    title: String,
    created: GraphQLDateTime,
    order: Int,
    visible: Int,
    images: [Image!],
    image: Image
  }
`;

export const Album = {
  id: ({ __album }) => __album.getDataValue('id'),
  title: ({ __album }) => __album.getDataValue('title'),
  created: ({ __album }) => __album.getDataValue('created'),
  order: ({ __album }) => __album.getDataValue('order'),
  visible: ({ __album }) => __album.getDataValue('visible'),

  images: async ({ __album }) => {
    const album = await __album.getImages();

    return album.map((image) => ({ __image: image }));
  },

  image: async ({ __album }) => {
    const __image = await __album.getImage();

    if (__image) {
      return { __image }
    }

    return null;
  }
};
