export const streamDef = `
 type Stream @cacheControl(maxAge: 604800) {
    id: Int!,
    title: String,
    created: GraphQLDateTime,
    type: Int,
    resourceId: Int
  }
`;

export const Stream = {
  id: ({ __stream }) => __stream.getDataValue('id'),
  title: ({ __stream }) => __stream.getDataValue('title'),
  created: ({ __stream }) => __stream.getDataValue('created'),
  type: ({ __stream }) => __stream.getDataValue('type'),
  resourceId: ({ __stream }) => __stream.getDataValue('resource_id'),
};
