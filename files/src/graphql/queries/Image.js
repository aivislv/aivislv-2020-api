export const imageDef = `
 type Image @cacheControl(maxAge: 604800) {
    id: Int!,
    title: String,
    persons: String,
    place: String,
    created: GraphQLDateTime,
    order: Int,
    visible: Int,
    extension: String,
    album: Album!,
    song: Song,
    session: Session, 
  }
`;

export const Image = {
    id: ({ __image }) => __image.getDataValue('id'),
    title: ({ __image }) => __image.getDataValue('title'),
    persons: ({ __image }) => __image.getDataValue('persons'),
    place: ({ __image }) => __image.getDataValue('place'),
    extension: ({ __image }) => __image.getDataValue('ext'),
    created: ({ __image }) => __image.getDataValue('created'),
    order: ({ __image }) => __image.getDataValue('order'),
    visible: ({ __image }) => __image.getDataValue('visible'),

    album: async ({ __image }) => {
        const __album = await __image.getAlbum();

        return { __album }
    },

    session: async ({ __image }) => {
        const __session = await __image.getSession();

        if (__session) {
            return { __session }
        } else {
            return null;
        }
    },

    song: async ({ __image }) => {
        const __song = await __image.getSong();

        if (__song) {
            return { __song }
        } else {
            return null;
        }
    }
};
