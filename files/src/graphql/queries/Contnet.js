export const contentDef = `
 type Content @cacheControl(maxAge: 604800) {
    id: Int!,
    title: String,
    titlePhoto: String,
    content: String,
  }
`;

export const Content = {
  id: ({ __content }) => __content.getDataValue('id'),
  title: ({ __content }) => __content.getDataValue('title'),
  titlePhoto: ({ __content }) => __content.getDataValue('title_photo'),
  content: ({ __content }) => __content.getDataValue('content'),
};
