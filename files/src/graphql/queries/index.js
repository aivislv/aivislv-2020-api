import { Query, queryDef } from './Query';
import { Stream, streamDef } from "./Stream";
import { Album, albumDef } from "./Album";
import { Image, imageDef } from "./Image";
import { Game, gameDef } from "./Game";
import { Anime, animeDef } from './Anime';
import { Book, bookDef } from './Book';
import { Movie, movieDef } from './Movie';
import { GamePlay, gamePlayDef } from './GamePlay';
import { Content, contentDef } from './Contnet';
import {Series, seriesDef} from './Series';
import {BookTag, bookTagDef} from './BookTag';
import {BookSeries, bookSeriesDef} from './BookSeries';
import { Author, authorDef } from './Author';
import { Session, sessionDef } from './Session';
import { Song, songDef } from './Song';
import { Tag, tagDef } from './Tag';
import { TagCategory, tagCategoryDef } from './TagCategory';

const queries = {
  Query,
  Stream,
  Album,
  Image,
  Game,
  Anime,
  Book,
  Movie,
  GamePlay,
  Content,
  Author,
  Series,
  BookSeries,
  BookTag,
  Song,
  Session,
  Tag,
  TagCategory,
};

const typeDef = [
  queryDef,
  streamDef,
  albumDef,
  imageDef,
  gameDef,
  animeDef,
  bookDef,
  movieDef,
  gamePlayDef,
  contentDef,
  authorDef,
  seriesDef,
  bookSeriesDef,
  songDef,
  sessionDef,
  tagDef,
  bookTagDef,
  tagCategoryDef,
];

export default { queries, typeDef };

