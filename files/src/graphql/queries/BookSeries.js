export const bookSeriesDef = `
 type BookSeries @cacheControl(maxAge: 604800) {
    ordered: Float,
    lastOrdered: Float,
    series: Series,
    book: Book,
  }
`;

export const BookSeries = {
  ordered: ({ __bookSeries }) => __bookSeries.getDataValue('ordered'),
  lastOrdered: ({ __bookSeries }) => __bookSeries.getDataValue('last_ordered'),
  series: async ({ __bookSeries }) => {
    if (__bookSeries.series) {
      return { __series: __bookSeries.series }
    } else {
      return { __series: await __bookSeries.getSeries() }
    }

  },
  book: async ({ __bookSeries }) => {
    if (__bookSeries.book) {
      return { __book: __bookSeries.book }
    } else {
      return { __book: await __bookSeries.getBook() }
    }

  },
};