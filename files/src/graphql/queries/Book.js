export const bookDef = `
 type Book @cacheControl(maxAge: 604800) {
    id: Int!,
    title: String,
    authors: [Author!],
    series: [BookSeries!],
    tags: [BookTag!],
    isRead: Int,
    isOwned: Int,
    language: String,
    isbn10: String,
    goodreadsId: Int,
    created: GraphQLDateTime,
    description: String,
    rating: Int,
    averageRating: Float,
    wishWeight: Int,
  }
`;

export const Book = {
  id: ({ __book }) => __book.getDataValue('id'),
  title: ({ __book }) => __book.getDataValue('title'),
  isRead: ({ __book }) => __book.getDataValue('isRead'),
  isOwned: ({ __book }) => __book.getDataValue('isOwned'),
  language: ({ __book }) => __book.getDataValue('lang'),
  isbn10: ({ __book }) => __book.getDataValue('isbn10'),
  created: ({ __book }) => __book.getDataValue('created'),
  description: ({ __book }) => __book.getDataValue('description'),
  goodreadsId: ({ __book }) => __book.getDataValue('goodreads_id'),
  rating: ({ __book }) => __book.getDataValue('rating'),
  wishWeight: ({__book}) => __book.getDataValue('wish_weight'),
  averageRating: ({ __book }) => __book.getDataValue('avg_rating'),

  authors: async ({ __book }) => {
    const authors = await __book.getAuthors();

    if (authors) {
      return (authors.map((author) => ({ __author: author.author })));
    } else {
      return null;
    }
  },

  series: async ({__book}) => {
    const series = await __book.getSeries();

    if (series) {
      return (series.map((serie) => ({__bookSeries: serie})));
    } else {
      return null;
    }
  },

  tags: async ({__book}) => {
    const series = await __book.getTags();

    if (series) {
      return (series.map((serie) => ({__bookTag: serie})));
    } else {
      return null;
    }
  },
};
