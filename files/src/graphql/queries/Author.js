export const authorDef = `
 type Author {
    id: Int!,
    name: String,
    goodreadsId: Int,
    booksLinked: Int,
    promote: Boolean,
    visible: Boolean,
    books: [Book!],
    series: [Series!],
  }
`;

export const Author = {
  id: ({__author}) => __author.getDataValue('id'),
  name: ({__author}) => __author.getDataValue('name_eng'),
  booksLinked: ({__author}) => __author.getDataValue('books_linked'),
  goodreadsId: ({__author}) => __author.getDataValue('goodreads_id'),
  promote: ({__author}) => __author.getDataValue('promote') === 1,
  visible: ({__author}) => __author.getDataValue('visible') === 1,

  books: async ({__author}) => {
    const books = await __author.getBooks();

    if (books) {
      return books.map((book) => ({__book: book.book}));
    } else {
      return null;
    }
  },

  series: async ({__author}) => {
    const series = await __author.getSeries();

    if (series) {
      return (series.map((serie) => ({__series: serie.series})));
    } else {
      return null;
    }
  },
};