export const sessionDef = `
 type Session @cacheControl(maxAge: 604800) {
    id: Int!,
    title: String,
    created: GraphQLDateTime,
    order: Int,
    promote: Int,
    images: [Image!],
    image: Image!
  }
`;

export const Session = {
  id: ({ __session }) => __session.getDataValue('id'),
  title: ({ __session }) => __session.getDataValue('title'),
  created: ({ __session }) => __session.getDataValue('created'),
  order: ({ __session }) => __session.getDataValue('ordered'),
  promote: ({ __session }) => __session.getDataValue('promote'),

  images: async ({ __session }) => {
    const images = await __session.getImages();

    return images.map((image) => ({ __image: image }));
  },

  image: async ({ __session }) => {
    const __image = await __session.getImage();

    return { __image }
  }
};
