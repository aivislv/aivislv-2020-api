import dayjs from 'dayjs';

export const movieDef = `
 type Movie @cacheControl(maxAge: 604800) {
    id: Int!,
    title: String,
    year: Int,
    status: Int,
    imdbId: String,
    score: Float,
    rating: Int,
    seen: GraphQLDateTime,
    seenUnix: Int,
    description: String,
  }
`;

export const Movie = {
  id: ({ __movie }) => __movie.getDataValue('id'),
  title: ({ __movie }) => __movie.getDataValue('title'),
  year: ({ __movie }) => __movie.getDataValue('year'),
  status: ({ __movie }) => __movie.getDataValue('status'),
  imdbId: ({ __movie }) => __movie.getDataValue('imdb_id'),
  score: ({ __movie }) => __movie.getDataValue('score'),
  rating: ({ __movie }) => __movie.getDataValue('rating'),
  seen: ({ __movie }) => __movie.getDataValue('seen'),
  seenUnix: ({ __movie }) => dayjs(__movie.getDataValue('seen')).unix(),
  description: ({ __movie }) => __movie.getDataValue('description'),
};
