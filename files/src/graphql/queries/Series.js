export const seriesDef = `
 type Series @cacheControl(maxAge: 604800) {
    id: Int!,
    title: String,
    goodreadsId: Int,
    booksLinked: Int,
    promote: Boolean,
    visible: Boolean,
    books: [BookSeries!],
    authors: [Author!],
    firstBookId: Int,
    tag: Boolean,
  }
`;

export const Series = {
  id: ({__series}) => __series.getDataValue('id'),
  title: ({__series}) => __series.getDataValue('title'),
  booksLinked: ({__series}) => __series.getDataValue('books_linked'),
  goodreadsId: ({__series}) => __series.getDataValue('goodreads_id'),
  promote: ({__series}) => __series.getDataValue('promote') === 1,
  visible: ({__series}) => __series.getDataValue('visible') === 1,
  tag: ({__series}) => __series.getDataValue('tag') === 1,

  firstBookId: async ({__series}) => {
    const book = await __series.getFirstBook();
    return book.book_id;
  },

  books: async ({__series}) => {
    const books = await __series.getBooks();

    if (books) {
      return (books.map((__bookSeries) => ({__bookSeries})));
    } else {
      return null;
    }
  },

  authors: async ({__series}) => {
    const authors = await __series.getAuthors();

    if (authors) {
      return (authors.map((author) => ({__author: author.author})));
    } else {
      return null;
    }
  },
};