export const songDef = `
 type Song @cacheControl(maxAge: 604800) {
    id: Int!,
    title: String,
    artist: String,
    spotifyId: String,
    youtubeLink: GraphQLURL,
    visible: Int,
    images: [Image!],
  }
`;

export const Song = {
  id: ({ __song }) => __song.getDataValue('id'),
  title: ({ __song }) => __song.getDataValue('title'),
  artist: ({ __song }) => __song.getDataValue('artist'),
  visible: ({ __song }) => __song.getDataValue('visible'),
  spotifyId: ({ __song }) => __song.getDataValue('spotifyId'),
  youtubeLink: ({ __song }) => __song.getDataValue('youtubeLink'),

  images: async ({ __song }) => {
    const images = await __song.getImages();

    return images.map((image) => ({ __image: image }));
  },
};
