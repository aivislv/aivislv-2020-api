import {
  Album,
  Image,
  Anime,
  Stream,
  Game,
  Movie,
  Book,
  Content,
  Session,
  Author,
  Series,
  GamePlay
} from '../../db/';
import GameTags from '../../db/GameTags';
import GameTagCategories from '../../db/GameTagCategories';

export const queryDef = `
 type Query @cacheControl(maxAge: 604800) {
    stream: [Stream!],

    gallery: [Album!],
    sessions: [Session!],
    
    album(albumId: Int): Album,
    image(imageId: Int!): Image,
    session(sessionId: Int!): Session,
    
    latestImages: [Image!],
    
    games: [Game!],
    game(gameId: Int!): Game,
    tags: [Tag!],
    tag(tagId: Int!): Tag,
    categories: [TagCategory!],
    
    content(contentId: Int!): Content,
                    
    animes: [Anime!],
    
    movies: [Movie!],
    
    books: [Book!],
    book(bookId: Int!): Book,
    
    authors: [Author!],
    author(authorId: Int!): Author,
    
    series: [Series!], 
    serie(seriesId: Int!): Series,   
    
    bookTags: [Series!],    
  }
`;

export const Query = {
  stream: async (parent, args) => {
    const streams = await Stream.findAll();

    return streams.map((__stream) => ({ __stream }));
  },

  tags: async (parent, args) => {
    const tags = await GameTags.findAll();

    return tags.map((__tag) => ({ __tag }));
  },

  tag: async (parent, args) => {
    const __tag = await GameTags.findByPk(args.tagId);

    if (__tag) {
      return { __tag };
    } else {
      return null;
    }
  },

  categories: async (parent, args) => {
    const cats = await GameTagCategories.findAll();

    return cats.map((__tagCategory) => ({ __tagCategory }));
  },

  gallery: async (parent, args) => {
    const gallery = await Album.findAll({
      where: { visible: 1 },
      order: [
        ['order', 'ASC'],
      ],
    });

    return gallery.map((__album) => ({ __album }));
  },

  sessions: async (parent, args) => {
    const sessions = await Session.findAll({
      where: { promote: 1 },
      order: [
        ['ordered', 'DESC'],
        ['created', 'DESC'],
      ],
    });

    return sessions.map((__session) => ({ __session }));
  },

  session: async (parent, args) => {
    const __session = await Session.findByPk(args.sessionId);

    if (__session) {
      return { __session };
    } else {
      return null;
    }
  },

  album: async (parent, args) => {
    const __album = await Album.findByPk(args.albumId);

    if (__album) {
      return { __album };
    } else {
      return null;
    }
  },

  image: async (parent, args) => {
    const __image = await Image.findByPk(args.imageId);

    if (__image) {
      return { __image };
    } else {
      return null;
    }
  },

  latestImages: async (parent, args) => {
    const imagesLatest = await Image.findAll({
      where: { visible: 1 },
      order: [
        ['id', 'DESC'],
      ],
      offset: 0, limit: 10
    });

    if (imagesLatest) {
      return imagesLatest.map((__image) => ({ __image }));
    } else {
      return null;
    }
  },

  content: async (parent, args) => {
    const __content = await Content.findByPk(args.contentId);

    return {
      __content,
    };
  },

  game: async (parent, args) => {
    const __game = await Game.findByPk(args.gameId, {
      include: [
        {
          model: GamePlay,
          required: false,
        },
        {
          model: Game,
          required: false,
          as: 'expansions'
        },
        {
          model: GameTags,
          required: false,
          as: 'tags'
        }
      ],
      order: [
        [GamePlay, 'date', 'DESC']
      ],
    });

    return {
      __game
    };
  },

  games: async (parent, args) => {
    const games = await Game.findAll({
      where: { expansion: 0 },
    });

    const gamePlays = await Game.findAll({
      where: {expansion: 0},
      include: [
        {
          model: GamePlay,
          required: true,
        }
      ],
      order: [
        [GamePlay, 'date', 'DESC']
      ],
    });

    const blankGame = () => ({gamePlays: [], expansions: [], tags: []})

    const gameData = {}

    gamePlays.forEach(play => {
      const gameId = play.getDataValue('id')

      if (!gameData[gameId]) {
        gameData[gameId] = blankGame()
      }

      gameData[gameId].gamePlays = play.gamePlays
    })

    const gameExpansions = await Game.findAll({
      where: {expansion: 0},
      include: [
        {
          model: Game,
          required: true,
          as: 'expansions'
        },
      ],
    })

    gameExpansions.forEach(expansion => {
      const gameId = expansion.getDataValue('id')

      if (!gameData[gameId]) {
        gameData[gameId] = blankGame()
      }

      gameData[gameId].expansions = expansion.expansions
    })

    const gameTags = await Game.findAll({
      where: {expansion: 0},
      include: [
        {
          model: GameTags,
          required: true,
          as: 'tags'
        }
      ],
    })

    gameTags.forEach(tag => {
      const gameId = tag.getDataValue('id')

      if (!gameData[gameId]) {
        gameData[gameId] = blankGame()
      }

      gameData[gameId].tags = tag.tags
    })

    return games.map((__game) => {
      if (gameData[__game.getDataValue('id')]) {
        __game.expansions = gameData[__game.getDataValue('id')].expansions;
        __game.tags = gameData[__game.getDataValue('id')].tags;
        __game.gamePlays = gameData[__game.getDataValue('id')].gamePlays;
      } else {
        __game.expansions = [];
        __game.tags = [];
        __game.gamePlays = [];
      }

      return {__game}
    });
  },

  animes: async (parent, args) => {
    const animes = await Anime.findAll();

    return animes.map((__anime) => ({ __anime }));
  },

  movies: async (parent, args) => {
    const movies = await Movie.findAll();

    return movies.map((__movie) => ({ __movie }));
  },

  books: async (parent, args) => {
    const books = await Book.findAll({
      where: { visible: 1 }
    });

    return books.map((__book) => ({ __book }));
  },

  book: async (parent, args) => {
    const __book = await Book.findByPk(args.bookId);

    return {
      __book
    };
  },

  series: async (parent, args) => {
    const series = await Series.findAll({
      where: {
        promote: 1,
        tag: 0,
      }
    });

    return series.map((__series) => ({ __series }));
  },

  serie: async (parent, args) => {
    const __series = await Series.findByPk(args.seriesId);

    return {
      __series
    };
  },

  bookTags: async (parent, args) => {
    const series = await Series.findAll({
      where: {
        promote: 1,
        tag: 1,
      }
    });

    return series.map((__series) => ({__series}));
  },

  authors: async (parent, args) => {
    const authors = await Author.findAll({
      where: {
        visible: 1
      }
    });

    return authors.map((__author) => ({__author}));
  },

  author: async (parent, args) => {
    const __author = await Author.findByPk(args.authorId);

    return {
      __author
    };
  }
};
