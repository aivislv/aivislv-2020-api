export const bookTagDef = `
 type BookTag @cacheControl(maxAge: 604800) {
    tag: Series,
    book: Book,
  }
`;

export const BookTag = {
    tag: async ({__bookTag}) => {
        if (__bookTag.series) {
            return {__series: __bookTag.series}
        } else {
            return {__series: await __bookTag.getSeries()}
        }

    },
    book: async ({__bookTag}) => {
        if (__bookTag.book) {
            return {__book: __bookTag.book}
        } else {
            return {__book: await __bookTag.getBook()}
        }
    },
};