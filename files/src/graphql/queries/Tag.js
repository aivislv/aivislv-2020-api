import dayjs from 'dayjs';

export const tagDef = `
 type Tag @cacheControl(maxAge: 604800) {
    id: Int!,
    tag: String!,
    promote: Boolean!,
    category: TagCategory!,
    categoryId: Int!,
    games: [Game!]
  }
`;

export const Tag = {
  id: ({ __tag }) => {
    return __tag.getDataValue('id');
  },
  tag: ({ __tag }) => __tag.getDataValue('tag'),
  promote: ({ __tag }) => __tag.getDataValue('promote'),

  categoryId: async ({ __tag }) => {
    return (await __tag.getGameTagCategories()).id;
  },

  category: async ({ __tag }) => {
    return { __tagCategory: await __tag.getGameTagCategory() };
  },

  games: async ({ __tag }) => {
    const games = await __tag.getGames();

    if (games) {
      return (games.map((game) => ({ __game: game.game })));
    } else {
      return null;
    }
  }
};
