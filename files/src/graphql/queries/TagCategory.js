import dayjs from 'dayjs';

export const tagCategoryDef = `
 type TagCategory @cacheControl(maxAge: 604800) {
    id: Int!,
    name: String!,
    mandatory: Boolean!,
    color: String,
    tags: [Tag!],
  }
`;

export const TagCategory = {
  id: ({ __tagCategory }) => __tagCategory.getDataValue('id'),
  name: ({ __tagCategory }) => __tagCategory.getDataValue('name'),
  mandatory: ({ __tagCategory }) => __tagCategory.getDataValue('mandatory'),
  color: ({ __tagCategory }) => __tagCategory.getDataValue('color'),

  tags: async ({ __tagCategory }) => {
    return __tagCategory.getTags().map((__tag) => ({ __tag }));
  }
};
