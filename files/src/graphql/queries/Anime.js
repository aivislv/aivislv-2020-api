export const animeDef = `
 type Anime @cacheControl(maxAge: 604800) {
    id: Int!,
    title: String,
    rating: Int,
    status: Int,
    malId: Int,
    score: Float,
    startDate: GraphQLDateTime,
    endDate: GraphQLDateTime,
    updated: Int,
    synopsis: String,
    wishWeight: Int,
  }
`;

export const Anime = {
  id: ({ __anime }) => __anime.getDataValue('id'),
  title: ({ __anime }) => __anime.getDataValue('title'),
  rating: ({ __anime }) => __anime.getDataValue('rating'),
  status: ({ __anime }) => __anime.getDataValue('status'),
  malId: ({ __anime }) => __anime.getDataValue('mal_id'),
  score: ({ __anime }) => __anime.getDataValue('score'),
  startDate: ({ __anime }) => __anime.getDataValue('start_date'),
  endDate: ({ __anime }) => __anime.getDataValue('end_date'),
  updated: ({ __anime }) => __anime.getDataValue('updated'),
  synopsis: ({ __anime }) => __anime.getDataValue('synopsis'),
  wishWeight: ({__anime}) => __anime.getDataValue('wish_weight'),
};
