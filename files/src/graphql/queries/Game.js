import dayjs from 'dayjs';

export const gameDef = `
 type Game @cacheControl(maxAge: 604800) {
    id: Int!,
    title: String,
    expansions: [Game!],
    parent: Game,
    bggId: Int,
    plays: Int,
    rating: Float,
    description: String,
    expansion: Int,
    minPlayers: Int,
    maxPlayers: Int,
    minPlaytime: Int,
    maxPlaytime: Int,
    playtime: Int,
    avgScore: Float,
    byeScore: Float,
    userCount: Int,
    rank: Int,
    type: Int,
    lastPlayUnix: Int,
    expansionCount: Int,
    lastPlay: GamePlay,
    allPlays: [GamePlay!],
    tags: [Tag!],
    players: String
    preordered: Boolean,
    wishlistPriority: Int,
    isAccessory: Boolean,
    isPromo: Boolean,
    wishWeight: Int,
  }
`;

export const Game = {
    id: ({ __game }) => __game.getDataValue('id'),
    title: ({ __game }) => __game.getDataValue('title'),
    bggId:  ({ __game }) => __game.getDataValue('bgg_id'),
    plays:  ({ __game }) => __game.getDataValue('plays'),
    rating:  ({ __game }) => __game.getDataValue('rating'),
    description:  ({ __game }) => __game.getDataValue('description'),
    expansion:  ({ __game }) => __game.getDataValue('expansion'),
    minPlayers: ({__game}) => __game.getDataValue('min_players'),
    maxPlayers: ({__game}) => __game.getDataValue('max_players'),
    minPlaytime: ({__game}) => __game.getDataValue('min_playtime'),
    maxPlaytime: ({__game}) => __game.getDataValue('max_playtime'),
    playtime: ({__game}) => __game.getDataValue('playtime'),
    avgScore: ({__game}) => __game.getDataValue('avg_score'),
    byeScore: ({__game}) => __game.getDataValue('bye_score'),
    userCount: ({__game}) => __game.getDataValue('usercount'),
    rank: ({__game}) => __game.getDataValue('rank'),
    type: ({__game}) => __game.getDataValue('type'),
    players: ({__game}) => __game.getDataValue('players'),
    preordered: ({__game}) => __game.getDataValue('preordered'),
    isAccessory: ({__game}) => __game.getDataValue('is_accessory'),
    isPromo: ({__game}) => __game.getDataValue('is_promo'),
    wishWeight: ({__game}) => __game.getDataValue('wish_weight'),
    wishlistPriority: ({__game}) => __game.getDataValue('wishlist_priority'),

    expansionCount: async ({__game}) => {
        if (__game.expansions) {
            return __game.expansions.length;
        } else {
            return null;
        }
    },

    expansions: async ({__game}) => {
        if (__game.expansions) {
            return __game.expansions.map((__game) => ({ __game }));
        } else {
            return null;
        }
    },

    parent: async ({ __game }) => {
        if (__game.getDataValue('expansion') !== 0) {
            const parent = await __game.getParent();
            return { __game: parent }
        }

        return null;
    },

    lastPlay: async ({ __game }) => {
        if (__game.gamePlays && __game.gamePlays[0]) {
            return { __gamePlay: __game.gamePlays[0] }
        } else {
            return null;
        }
    },

    lastPlayUnix: async ({ __game }) => {
        if (__game.gamePlays && __game.gamePlays[0]) {
            return dayjs(__game.gamePlays[0].getDataValue('date')).unix();
        } else {
          return null;
        }
    },

  allPlays: async ({ __game }) => {
    if (__game.gamePlays) {
      return __game.gamePlays.map((__gamePlay) => ({ __gamePlay }));
    } else {
      return null;
    }
  },

  tags: async ({ __game }) => {
      if (__game.tags) {
          return __game.tags.map((__tag) => ({__tag}));
      } else {
          return null;
      }
  }
};
