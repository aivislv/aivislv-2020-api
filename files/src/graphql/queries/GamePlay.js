export const gamePlayDef = `
 type GamePlay @cacheControl(maxAge: 604800) {
    id: Int!,
    gameId: Int!,
    date: GraphQLDateTime!,
    quantity: Int!
  }
`;

export const GamePlay = {
  id: ({ __gamePlay }) => __gamePlay.getDataValue('id'),
  gameId: ({ __gamePlay }) => __gamePlay.getDataValue('game_id'),
  date: ({ __gamePlay }) => __gamePlay.getDataValue('date'),
  quantity: ({ __gamePlay }) => __gamePlay.getDataValue('quantity'),
};
