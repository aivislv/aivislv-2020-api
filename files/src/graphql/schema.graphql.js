import { makeExecutableSchema } from 'graphql-tools';

// import dbRelations from './relations/index';
import queries from './queries/index';
import scalars from './scalars/index';

// import dbMutations from './mutations/index';

export const resolvers = {
  ...queries.queries,
  ...scalars.queries,
};

export const typeDefs = [...queries.typeDef, ...scalars.typeDef];
