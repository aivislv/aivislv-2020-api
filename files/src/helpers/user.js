exports.createUser = (session) => {
  if (session.user === void(0)) {
    session.user = Math.round(Math.random() * 100000);
  }

  return session.user;
};
