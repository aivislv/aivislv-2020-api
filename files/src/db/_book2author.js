import Sequelize from 'sequelize';
import { getConnection } from '../init';

class _book2author extends Sequelize.Model {}
const connection = getConnection();

_book2author.init({
  author_id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
  book_id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
}, {
  sequelize: connection,
  modelName: 'book2author',
  freezeTableName: true,
  tableName: 'book2author',
  timestamps: false,
});

export default _book2author;
