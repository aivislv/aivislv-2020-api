import Sequelize from 'sequelize';
import { getConnection } from '../init';
import Image from './Image';

class Album extends Sequelize.Model {}
const connection = getConnection();

Album.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    title: Sequelize.STRING,
    created: Sequelize.STRING,
    order: Sequelize.INTEGER,
    visible: Sequelize.INTEGER,
    image: Sequelize.INTEGER,
}, {
    sequelize: connection,
    modelName: 'albums',
    freezeTableName: true,
    tableName: 'albums',
    timestamps: false,
});

Album.prototype.getImage = function () {
    return Image.findByPk(this.getDataValue('image'))
        .then((image) => {
            return image;
        });
};

Album.prototype.getImages = function () {
    return Image.findAll({
        where: { gid: this.getDataValue('id'), visible: 1 },
        order: [
            ['order', 'DESC'],
        ],
    }).then((images) => {
        return images;
    });
};


export default Album;
