import Sequelize from 'sequelize';
const Op = Sequelize.Op
import { getConnection } from '../init';
import Book from './Book';
import _book2series from './_book2series';
import GamePlay from './GamePlay';
import Author from "./Author";

class Series extends Sequelize.Model {}
const connection = getConnection();

Series.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  title: Sequelize.STRING,
  goodreads_id: Sequelize.INTEGER,
  books_linked: Sequelize.INTEGER,
  visible: Sequelize.INTEGER,
  promote: Sequelize.INTEGER,
  tag: Sequelize.INTEGER,
}, {
  sequelize: connection,
  modelName: 'series',
  freezeTableName: true,
  tableName: 'book_series',
  timestamps: false,
});

export default Series;

Series.prototype.getBooks = function () {
  return _book2series.findAll({
    where: {
      series_id: this.getDataValue('id'),
      '$book.visible$': 1,
    },
    include: [{
      model: Book,
    }]
  }).then((book) => {
    return book;
  });
};

Series.prototype.getFirstBook = function () {
  return _book2series.findOne({
    where: {
      series_id: this.getDataValue('id'),
    },
    order: [
      ['ordered', 'ASC']
    ],
  }).then((book) => {
    return book;
  });
};

Series.prototype.getAuthors = async function () {
  const books = await this.getBooks();

  const seriesList = [];

  for (let book in books) {
    const bookSeries = await books[book].book.getAuthors();

    for (let bookSerie in bookSeries) {
      let has = false;

      for (let authorSerie in seriesList) {
        if (seriesList[authorSerie].author_id === bookSeries[bookSerie].author_id) {
          has = true
        }
      }

      if (!has) {
        seriesList.push(bookSeries[bookSerie])
      }
    }
  }

  return seriesList;
}
