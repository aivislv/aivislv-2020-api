import Sequelize from 'sequelize';
import { getConnection } from '../init';
import GamePlay from './GamePlay';
import _game2tag from './_game2tag';
import GameTags from './GameTags';

class Game extends Sequelize.Model {
}

const connection = getConnection();

Game.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    title: Sequelize.STRING,
    bgg_id: Sequelize.INTEGER,
    plays: Sequelize.INTEGER,
    expansion: Sequelize.INTEGER,
    rating: Sequelize.DOUBLE,
    description: Sequelize.STRING,
    min_players: Sequelize.INTEGER,
    max_players: Sequelize.INTEGER,
    min_playtime: Sequelize.INTEGER,
    max_playtime: Sequelize.INTEGER,
    playtime: Sequelize.INTEGER,
    avg_score: Sequelize.DOUBLE,
    bye_score: Sequelize.DOUBLE,
    usercount: Sequelize.INTEGER,
    rank: Sequelize.INTEGER,
    type: Sequelize.INTEGER,
    players: Sequelize.STRING,
    preordered: Sequelize.BOOLEAN,
    wishlist_priority: Sequelize.INTEGER,
    wish_weight: Sequelize.INTEGER,
    is_promo: Sequelize.BOOLEAN,
    is_accessory: Sequelize.BOOLEAN,
}, {
    sequelize: connection,
    modelName: 'games',
    freezeTableName: true,
    tableName: 'games',
    timestamps: false,
});

Game.prototype.getExpansions = function () {
    return Game.findAll({
        where: { expansion: this.getDataValue('bgg_id') }
    }).then((games) => {
        return games;
    });
};

Game.prototype.getParent = function () {
    return Game.findByPk(this.getDataValue('expansion'))
        .then((image) => {
            return image;
        });
};

Game.prototype.getLastPlay = function () {
    return GamePlay.findOne({
        where: { game_id: this.getDataValue('id') },
        order: [
            ['date', 'DESC'],
        ]
    }).then((gamePlay) => {
        return gamePlay;
    })
};

Game.prototype.getAllPlays = function () {
    return GamePlay.findAll({
        where: { game_id: this.getDataValue('id') },
        order: [
            ['date', 'DESC'],
        ]
    }).then((gamePlay) => {
        return gamePlay;
    })
};

Game.prototype.getTags = function () {
    return _game2tag.findAll({
        where: {
            game_id: this.getDataValue('id')
        },
        include: [{
            model: GameTags,
        }]
    }).then((tags) => {
        return tags;
    });
};

export default Game;
