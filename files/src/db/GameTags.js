import Sequelize from 'sequelize';
import { getConnection } from '../init';
import GameTagCategories from './GameTagCategories';
import Book from './Book';
import _game2tag from './_game2tag';
import Game from './Game';

class GameTags extends Sequelize.Model {
}

const connection = getConnection();

GameTags.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    tag: Sequelize.STRING,
    promote: Sequelize.INTEGER,
    category_id: Sequelize.INTEGER,

}, {
    sequelize: connection,
    modelName: 'gametags',
    freezeTableName: true,
    tableName: 'game_tags',
    timestamps: false,
});

GameTags.prototype.getGameTagCategory = function () {
    return GameTagCategories.findOne({
        where: { id: this.getDataValue('category_id') }
    }).then((cats) => {
        return cats;
    });
};

GameTags.prototype.getGames = function () {
    return _game2tag.findAll({
        where: {
            tag_id: this.getDataValue('id')
        },
        include: [{
            model: Game,
        }]
    }).then((games) => {
        return games;
    });
};

export default GameTags;
