import Sequelize from 'sequelize';
import { getConnection } from '../init';
import Image from './Image';

class Song extends Sequelize.Model {}
const connection = getConnection();

Song.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  title: Sequelize.STRING,
  artist: Sequelize.STRING,
  visible: Sequelize.INTEGER,
  youtube_link: Sequelize.STRING,
  spotify_id: Sequelize.STRING,
}, {
  sequelize: connection,
  modelName: 'songs',
  freezeTableName: true,
  tableName: 'songs',
  timestamps: false,
});

Song.prototype.getImages = function () {
  return Image.findAll({
    where: { song_id: this.getDataValue('id') },
    order: [
      ['order', 'DESC'],
    ],
  }).then((images) => {
    return images;
  });
};


export default Song;
