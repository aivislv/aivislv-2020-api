import Sequelize from 'sequelize';
import { getConnection } from '../init';
import Book from './Book';
import Series from './Series';
import GameTags from './GameTags';

class _game2tag extends Sequelize.Model {
}

const connection = getConnection();

_game2tag.init({
  game_id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
  tag_id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
}, {
  sequelize: connection,
  modelName: 'games2tags',
  freezeTableName: true,
  tableName: 'games2tags',
  timestamps: false,
});

export default _game2tag;

_game2tag.prototype.getBook = function () {
  return Game.findByPk(this.game_id)
    .then((game) => {
      return game;
    });
};

_game2tag.prototype.getSeries = function () {
  return GameTags.findByPk(this.tag_id)
    .then((tag) => {
      return tag;
    });
};