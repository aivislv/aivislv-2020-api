import Sequelize from 'sequelize';
import { getConnection } from '../init';

class PledgeLevel extends Sequelize.Model {}
const connection = getConnection();

BackerPledge.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
}, {
  sequelize: connection,
  modelName: '',
  freezeTableName: true,
  tableName: '',
  timestamps: false,
});

export default BackerPledge;
