import Sequelize from 'sequelize';
import { getConnection } from '../init';

class GamePlay extends Sequelize.Model {}
const connection = getConnection();

GamePlay.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  game_id: Sequelize.INTEGER,
  date: Sequelize.STRING,
  quantity: Sequelize.INTEGER,

}, {
  sequelize: connection,
  modelName: 'gamePlay',
  freezeTableName: true,
  tableName: 'games_plays',
  timestamps: false,
});

export default GamePlay;
