import Sequelize from 'sequelize';
import { getConnection } from '../init';

class Content extends Sequelize.Model {}
const connection = getConnection();

Content.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    title: Sequelize.STRING,
    title_photo: Sequelize.STRING,
    content: Sequelize.STRING,
}, {
    sequelize: connection,
    modelName: 'content',
    freezeTableName: true,
    tableName: 'about',
    timestamps: false,
});

export default Content;
