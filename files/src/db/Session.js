import Sequelize from 'sequelize';
import { getConnection } from '../init';
import Image from './Image';

class Session extends Sequelize.Model {}
const connection = getConnection();

Session.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  title: Sequelize.STRING,
  created: Sequelize.STRING,
  ordered: Sequelize.INTEGER,
  promote: Sequelize.INTEGER,
  title_image_id: Sequelize.INTEGER,
}, {
  sequelize: connection,
  modelName: 'session',
  freezeTableName: true,
  tableName: 'photo_session',
  timestamps: false,
});

Session.prototype.getImage = function () {
  return Image.findByPk(this.getDataValue('title_image_id'))
    .then((image) => {
      return image;
    });
};

Session.prototype.getImages = function () {
  return Image.findAll({
    where: { session_id: this.getDataValue('id') },
    order: [
      ['order', 'DESC'],
    ],
  }).then((images) => {
    return images;
  });
};


export default Session;
