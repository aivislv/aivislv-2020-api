import Sequelize from 'sequelize';
import { getConnection } from '../init';
import Album from './Album';
import Song from './Song';
import Session from './Session';

class Image extends Sequelize.Model {}
const connection = getConnection();

Image.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    title: Sequelize.STRING,
    created: Sequelize.STRING,
    persons: Sequelize.STRING,
    place: Sequelize.STRING,
    ext: Sequelize.STRING,
    order: Sequelize.INTEGER,
    visible: Sequelize.INTEGER,
    gid: Sequelize.INTEGER,
    session_id: Sequelize.INTEGER,
    song_id: Sequelize.INTEGER,
}, {
    sequelize: connection,
    modelName: 'images',
    freezeTableName: true,
    tableName: 'images',
    timestamps: false,
});

Image.prototype.getAlbum = function () {
    return Album.findByPk(this.getDataValue('gid'))
        .then((image) => {
            return image;
        });
};

Image.prototype.getSession = function () {
    return Session.findByPk(this.getDataValue('session_id'))
      .then((session) => {
          return session;
      });
};

Image.prototype.getSong = function () {
    return Song.findByPk(this.getDataValue('song_id'))
      .then((song) => {
          return song;
      });
};

export default Image;
