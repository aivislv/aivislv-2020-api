import Sequelize from 'sequelize';
import { getConnection } from '../init';
import Book from './Book';
import Series from './Series';

class _book2series extends Sequelize.Model {}
const connection = getConnection();

_book2series.init({
  series_id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
  book_id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
  },
  ordered: Sequelize.FLOAT,
  last_ordered: Sequelize.FLOAT,
}, {
  sequelize: connection,
  modelName: 'book2series',
  freezeTableName: true,
  tableName: 'book2series',
  timestamps: false,
});

export default _book2series;

_book2series.prototype.getBook = function () {
  return Book.findByPk(this.book_id)
    .then((book) => {
      return book;
    });
}

_book2series.prototype.getSeries = function () {
  return Series.findByPk(this.series_id)
    .then((series) => {
      return series;
    });
}