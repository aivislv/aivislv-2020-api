import Sequelize from 'sequelize';
import { getConnection } from '../init';

class Movie extends Sequelize.Model {}
const connection = getConnection();

Movie.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    title: Sequelize.STRING,
    status: Sequelize.INTEGER,
    imdb_id: Sequelize.INTEGER,
    seen: Sequelize.STRING,
    description: Sequelize.STRING,
    year: Sequelize.INTEGER,
    score: Sequelize.FLOAT,
    rating: Sequelize.INTEGER,
}, {
    sequelize: connection,
    modelName: 'movies',
    freezeTableName: true,
    tableName: 'movies',
    timestamps: false,
});

export default Movie;
