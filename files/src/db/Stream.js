import Sequelize from 'sequelize';
import { getConnection } from '../init';

class Stream extends Sequelize.Model {}
const connection = getConnection();

Stream.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    title: Sequelize.STRING,
    created: Sequelize.STRING,
    type: Sequelize.INTEGER,
    resource_id: Sequelize.INTEGER,

}, {
    sequelize: connection,
    modelName: 'stream',
    freezeTableName: true,
    tableName: 'stream',
    timestamps: false,
});

export default Stream;
