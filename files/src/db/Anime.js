import Sequelize from 'sequelize';
import { getConnection } from '../init';

class Anime extends Sequelize.Model {}
const connection = getConnection();

Anime.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    title: Sequelize.STRING,
    rating: Sequelize.INTEGER,
    status: Sequelize.INTEGER,
    mal_id: Sequelize.INTEGER,
    score: Sequelize.DOUBLE,
    start_date: Sequelize.STRING,
    end_date: Sequelize.STRING,
    synopsis: Sequelize.STRING,
    updated: Sequelize.INTEGER,
    wish_weight: Sequelize.INTEGER,
}, {
    sequelize: connection,
    modelName: 'animes',
    freezeTableName: true,
    tableName: 'animes',
    timestamps: false,
});

export default Anime;
