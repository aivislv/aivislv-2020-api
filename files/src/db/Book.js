import Sequelize from 'sequelize';
import { getConnection } from '../init';
import _book2series from './_book2series';
import Series from './Series';
import _book2author from './_book2author';
import Author from './Author';

class Book extends Sequelize.Model {}
const connection = getConnection();

Book.init({
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    title: Sequelize.STRING,
    lang: Sequelize.STRING,
    isbn10: Sequelize.STRING,
    created: Sequelize.STRING,
    isRead: Sequelize.INTEGER,
    isOwned: Sequelize.INTEGER,
    goodreads_id: Sequelize.INTEGER,
    description: Sequelize.STRING,
    rating: Sequelize.INTEGER,
    avg_rating: Sequelize.FLOAT,
    visible: Sequelize.INTEGER,
    wish_weight: Sequelize.INTEGER,
}, {
    sequelize: connection,
    modelName: 'books',
    freezeTableName: true,
    tableName: 'books',
    timestamps: false,
});

export default Book;

Book.prototype.getSeries = function () {
    return _book2series.findAll({
        where: {
            book_id: this.getDataValue('id'),
            '$series.visible$': 1,
            '$series.tag$': 0,
        },
        include: [{
            model: Series,
        }]
    }).then((series) => {
        return series;
    });
};

Book.prototype.getTags = function () {
    return _book2series.findAll({
        where: {
            book_id: this.getDataValue('id'),
            '$series.visible$': 1,
            '$series.tag$': 1,
        },
        include: [{
            model: Series,
        }]
    }).then((series) => {
        return series;
    });
};

Book.prototype.getAuthors = function () {
    return _book2author.findAll({
        where: {
            book_id: this.getDataValue('id'),
            '$author.visible$': 1,
        },
        include: [{
            model: Author,
        }]
    }).then((author) => {
        return author;
    });
};