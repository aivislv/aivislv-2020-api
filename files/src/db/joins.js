import { getConnection } from '../init';
import GamePlay from './GamePlay';
import Game from './Game';
import Book from './Book';
import Author from './Author';
import Series from './Series';
import _book2author from './_book2author';
import _book2series from './_book2series';
import _game2tag from './_game2tag';
import GameTags from './GameTags';

const connection = getConnection();

Game.hasMany(GamePlay, {
  foreignKey: 'game_id',
});

GamePlay.belongsTo(Game, {
  foreignKey: 'game_id',
});

Game.hasMany(Game, {
  foreignKey: 'expansion',
  sourceKey: 'bgg_id',
  as: 'expansions',
});

Game.belongsTo(Game, {
  foreignKey: 'expansion',
  sourceKey: 'bgg_id',
  as: 'parent',
});

// --------- >>> Book 2 Author

Book.hasMany(_book2author,  {
  foreignKey: 'book_id',
});

_book2author.belongsTo(Book, {
  foreignKey: 'book_id',
});

Author.hasMany(_book2author,  {
  foreignKey: 'author_id',
});

_book2author.belongsTo(Author, {
  foreignKey: 'author_id',
});

// --------- >>> Book 2 Series

Book.hasMany(_book2series,  {
  foreignKey: 'book_id',
});

_book2series.belongsTo(Book, {
  foreignKey: 'book_id',
});

Series.hasMany(_book2series, {
  foreignKey: 'series_id',
});

_book2series.belongsTo(Series, {
  foreignKey: 'series_id',
});

// --------- >>> Game 2 Tags

Game.belongsToMany(GameTags, {
  through: _game2tag,
  foreignKey: 'game_id',
  as: "tags"
});

GameTags.belongsToMany(Game, {
  through: _game2tag,
  foreignKey: 'tag_id',
  as: "games"
});