import Sequelize from 'sequelize';
import { getConnection } from '../init';
import GamePlay from './GamePlay';
import GameTags from './GameTags';

class GameTagCategories extends Sequelize.Model {
}

const connection = getConnection();

GameTagCategories.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  name: Sequelize.STRING,
  mandatory: Sequelize.INTEGER,
  color: Sequelize.STRING,
}, {
    sequelize: connection,
    modelName: 'gametagcats',
    freezeTableName: true,
    tableName: 'game_tag_categories',
    timestamps: false,
});

GameTagCategories.prototype.getTags = function () {
    return GameTags.findAll({
        where: { category_id: this.getDataValue('id') }
    }).then((cats) => {
        return cats;
    });
};

export default GameTagCategories;
