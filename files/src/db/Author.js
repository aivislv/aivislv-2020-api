import Sequelize from 'sequelize';
import { getConnection } from '../init';
import _book2author from './_book2author';
import Book from './Book';

class Author extends Sequelize.Model {}
const connection = getConnection();

Author.init({
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  name_eng: Sequelize.STRING,
  goodreads_id: Sequelize.INTEGER,
  books_linked: Sequelize.INTEGER,
  promote: Sequelize.INTEGER,
  visible: Sequelize.INTEGER,
}, {
  sequelize: connection,
  modelName: 'authors',
  freezeTableName: true,
  tableName: 'book_authors',
  timestamps: false,
});

export default Author;

Author.prototype.getBooks = function () {
  return _book2author.findAll({
    where: {
      author_id: this.getDataValue('id'),
      '$book.visible$': 1,
    },
    include: [{
      model: Book,
    }]
  }).then((books) => {
    return books;
  });
};

Author.prototype.getSeries = async function () {
  const books = await this.getBooks();

  const seriesList = [];

  for (let book in books) {
    const bookSeries = await books[book].book.getSeries();

    for (let bookSerie in bookSeries) {
      let has = false;

      for (let authorSerie in seriesList) {
        if (seriesList[authorSerie].series_id === bookSeries[bookSerie].series_id) {
          has = true
        }
      }

      if (!has) {
        seriesList.push(bookSeries[bookSerie])
      }
    }
  }

  return seriesList;
}