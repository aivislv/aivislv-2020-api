import Sequelize from 'sequelize';
import Cache from 'memcached-promisify';

let db = false;
let cache = false;

if (!db) {
  db = new Sequelize(
    `mysql://${process.env.MYSQL_USER}:${process.env.MYSQL_PASS}@${process.env.MYSQL_HOST}:${process.env.MYSQL_PORT}/${process.env.MYSQL_DB}`, {
      // disable logging; default: console.log
      logging: false
    });
}

if (!cache) {
  cache = new Cache({ cacheHost: `${process.env.MEMC_HOST}:${process.env.MEMC_PORT}`});
}

export const getConnection = () => {
  return db;
};

export const getCache = () => {
  return cache;
};

export const closeConnection = () => {
  db.close();
}
