import express from 'express';
import dotenv from 'dotenv';
import responseCachePlugin from 'apollo-server-plugin-response-cache';
import { MemcachedCache } from 'apollo-server-cache-memcached';

import { ApolloServer } from 'apollo-server-express';
import { typeDefs, resolvers } from './graphql/schema.graphql';

import cors from 'cors';

import { getConnection, getCatche } from './init';

dotenv.config();

// Set variables
const PORT = 8888;

console.info('Server starting!');

const server = new ApolloServer({
  typeDefs,
  resolvers,
  cors: false,
  plugins: [
    responseCachePlugin(
      {
        cache: new MemcachedCache(
          ['a-memcached'],
          { retries: 10, retry: 10000 }
        ),
      }
    )
  ]
});
const app = express();

app.use(cors({
  origin: '*',
}));

server.applyMiddleware({
  path: '/',
  app,
});

app.listen({ port: PORT }, () =>
  console.log(`Server ready at http://localhost:${PORT}${server.graphqlPath}`)
);
